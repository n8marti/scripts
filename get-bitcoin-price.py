#!/usr/bin/env python3
"""
Get the current price in USD of 1 BTC using the CoinDesk API.
"""

import json
import urllib.request

"""
CoinDesk provides a simple API to make its Bitcoin Price Index (BPI) data
programmatically available to others. You are free to use this API to include
our data in any application or website as you see fit, as long as each page or
app that uses it includes the text “Powered by CoinDesk”, linking to our price
page. CoinDesk data is made available through a number of HTTP resources, and
data is returned in JSON format. Please do not abuse our service.
"""

# Current price.
"""
LINK:
https://api.coindesk.com/v1/bpi/currentprice.json

JSON RESPONSE:
{
    "time":{
        "updated":"Sep 18, 2013 17:27:00 UTC",
        "updatedISO":"2013-09-18T17:27:00+00:00"
    },
    "disclaimer":"This data was produced from the CoinDesk Bitcoin Price Index. Non-USD currency data converted using hourly conversion rate from openexchangerates.org",
    "bpi":{
        "USD":{
            "code":"USD","symbol":"$","rate":"126.5235","description":"United States Dollar","rate_float":126.5235
        },
        "GBP":{
            "code":"GBP","symbol":"£","rate":"79.2495","description":"British Pound Sterling","rate_float":79.2495
        },
        "EUR":{
            "code":"EUR","symbol":"€","rate":"94.7398","description":"Euro","rate_float":94.7398
        }
    }
}
"""

# Get the current price.
url = "https://api.coindesk.com/v1/bpi/currentprice.json"
with urllib.request.urlopen(url) as r:
    data = json.loads(r.read().decode())

time = data.get("time").get("updated")
#usd_sym = data.get("bpi").get("USD").get("symbol")
note = data.get("disclaimer")
usd_rate = data.get("bpi").get("USD").get("rate")
print(f"1 BTC = ${usd_rate} at {time}")
print(f"\n{note}")


# Historical close prices.
"""
LINK:
https://api.coindesk.com/v1/bpi/historical/close.json?start=2013-09-01&end=2013-09-05

JSON RESPONSE:
{
    "bpi":{
        "2013-09-01":128.2597,
        "2013-09-02":127.3648,
        "2013-09-03":127.5915,
        "2013-09-04":120.5738,
        "2013-09-05":120.5333
    },
    "disclaimer":"This data was produced from the CoinDesk Bitcoin Price Index. BPI value data returned as USD.",
    "time":{
        "updated":"Sep 6, 2013 00:03:00 UTC",
        "updatedISO":"2013-09-06T00:03:00+00:00"
    }
}
"""
