#!/bin/bash

# Get the speedtest.csv file from servacatba for local inspection.

lan_ip="172.16.1.4"
vpn_ip="10.16.1.4"
source="/home/acatbadmin/Partages/apps-etc/speedtest.csv"
destination="$HOME"

# Use LAN connection if on ACATBA LAN, otherwise use VPN connection.
gateway=$(ip route | grep default | sed -r 's/.* via ([0-9.]{7,}) .*/\1/')
if [[ $gateway == '172.16.1.1' ]]; then
    ip=$lan_ip
else
    ip=$vpn_ip
fi

rsync --progress "acatbadmin@${ip}:${source}" "$destination"
