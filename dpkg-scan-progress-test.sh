#!/bin/bash

# Goals
# ------------------------------------------------------------------------------
# - Compare number of packages in Packages.0 file with number of packages in folder.
# - Assume both are in the same parent folder.
# - Show an incremental progress bar.


convertsecs() {
 ((h=${1}/3600))
 ((m=(${1}%3600)/60))
 ((s=${1}%60))
 printf "%02d:%02d\n" $m $s
}

# Some setup for test configuration.
# ------------------------------------------------------------------------------
# Create a test directory if it doesn't already exist.
test_dir="$HOME/dpkg-scan-test"
if [[ ! -d $test_dir ]]; then
    mkdir "$test_dir"
fi

# Populate the test directory with some deb files.
if [[ -z $(find "$test_dir" -type f) ]]; then
    cp /var/cache/apt/archives/lib* "$test_dir"
fi


# Processing
# ------------------------------------------------------------------------------
# dpkg-scannpackages creates temp files called /tmp/dpkg-deb.XXXXXX via
#   subprocesses for each deb scanned, but they come and go so fast I can't
#   seem to capture or count them. Instead, this script runs dpkg-scanpackages
#   through strace so that a pid file is created for each deb package scanned.
#   Then these pid files are counted to gauge progress.

# Create a tempfile location.
tmpf="wasta-offline"

# Set options for strace command.
# -ff   write each process to <filename>.pid, where <filename> comes from -o arg
# -o    output file
# -e    filter expression
o=("-ff" "-o" "/tmp/$tmpf" "-e" "trace=none")

# Run dpkg-scanpackages within strace to capture spawned subprocesses; send to bg.
strace "${o[@]}" -f dpkg-scanpackages --multiversion "$test_dir" > "$test_dir"/Packages.0 2>/dev/null &
pid=$!

# Count packages in test directory.
qty_to_scan=$(find "$test_dir" -name '*.deb' | wc -l)
if [[ $qty_to_scan -eq 0 ]]; then
    echo "No packages to scan. Exiting."
    exit 1
fi

# Count pid files spawned by strace as a proxy for # of debs scanned.
#   Empirically, p=n*5+1, where p is number of pids and n is number of packages.
#   Rearranged, n = (p-1)/5
time_start=$(date +%s)
time_remaining=
ct=1
while [[ $(ps --no-headers -p "$pid") ]]; do
    qty_pids=$(find /tmp -name "$tmpf"'*' 2>/dev/null | wc -l)
    # Calculate number of deb files from found pids.
    qty_scanned=$(( ($qty_pids - 1) / 5 ))
    qty_remaining=$(( $qty_to_scan - $qty_scanned ))
    percent=$(( $qty_scanned * 100 / $qty_to_scan ))

    time_elapsed=$(( $(date +%s) - $time_start ))
    if [[ $qty_scanned -gt $(( $qty_to_scan / 2 )) ]] && [[ $time_elapsed -gt 0 ]]; then
        if [[ $(( $time_elapsed % 2 )) -eq 0 ]]; then
            scan_rate=$(( $qty_scanned / $time_elapsed )) # pkgs/sec
            #time_remaining=$(( $qty_remaining / $scan_rate ))
            time_remaining=$(convertsecs $(( $qty_remaining / $scan_rate )))
        fi
        est="$time_remaining remaining..."
    else
        est=""
    fi

    echo "$percent"
    echo "#${qty_scanned}/${qty_to_scan} scanned ($percent%)\n$est"
    sleep 0.2
    ct=$(( $ct + 1 ))
done | zenity --progress --auto-close 2>/dev/null

# Remove tempfiles.
rm -f "/tmp/${tmpf}".*
exit 0
