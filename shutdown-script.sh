#!/bin/bash

#### Rerun script every 10 minutes using crontab.
# TODO: put crontab script here for reference?

#### Set initial variables ################################################
script="$0"
script_name="${script##*/}"
script_dir="${script%/*}"
mkdir -p "/home/$USER/bin/log"
log="$script_dir/log/${script_name%.*}.log"

now=$(date +%c)
nic_in_use=wlp4s0
net_state_location="/sys/class/net/$nic_in_use/operstate"


#### Main Processing ######################################################

## Check to see if there's a network connection. If so, write "up" to external file, otherwise write "down".

# Get previous network state and assign to variable.
touch /tmp/prev-network-state
prev_network_state=$(cat /tmp/prev-network-state)

# Get current network state, assign to a variable, and write to file as "previous network state".
network_state=$(cat $net_state_location)
echo $network_state > /tmp/prev-network-state

# Write variables to log file
#echo "shutdown-script.bash run at $now" >> $log
echo "Network connection is $network_state."
#echo "network is $network_state." >> $log
#echo >> $log
if [[ $network_state == "up" ]]
	then
		action="Action: none"
		echo "State noted, quitting script."
		#echo "$now	| Network: $network_state	| $action" >> $log
		exit 10
fi

### If current state is "down", check previous state. If it was "up", then warn that shutdown will happen next time.
if [[ $prev_network_state == "up" ]]
	then
		action="Action: No network. Waiting for next run..."
		echo "Network connection was up last time but is down now. If"
		echo "connection is down at next check, then the computer will be"
		echo "shut down."
		echo "$now	| Network: $network_state	| $action" >> $log
		exit 11
fi

### If the previous state was "down", then shutdown (two consecutive downs, 10min apart).
if [[ $prev_network_state == "down" ]]
	then
		action="Action: shutting down"
		echo "Shutting down in 1 minute."
		echo "$now	| Network: $network_state	| $action" >> $log
		#pm-hibernate
		/sbin/shutdown +1
		exit 12
fi

echo
echo "Network is down at first run of script."
#read -p "Click [Enter] to quit"
exit 11
