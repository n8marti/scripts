#!/usr/bin/env python3

"""
Emulate rolling two dice. Output the result as console art, e.g.:
    +-------+   +-------+
    | *   * |   |       |
    | *   * |   |   *   |
    | *   * |   |       |
    +-------+   +-------+
"""

import random
import sys


def get_digit():
    # Return a single digit from 1 to 6.
    digit = random.randint(1, 6)
    return digit

def draw_dice(digits):
    """
    'a' and 'b' are integers representing the value of each die rolled.
    """
    # List all possible dice faces in numerical order.
    poss_faces = [one, two, three, four, five, six]
    # Create a list of 2 faces based on the given integers 'a' and 'b'.
    faces = []
    for d in digits:
        faces.append(poss_faces[d - 1])
    # Draw the faces side by side, one band at a time, from band 0 to band 4.
    for band in range(5):
        # print(f"\t{faces[0][band]}\t{faces[1][band]}")
        print('\t'.join([faces[i][band] for i in range(len(faces))]))


# These are the unique "bands" needed to produce all possible die faces:
# +-------+     edge
# |       |     blank
# |     * |     right
# |   *   |     middle
# | *     |     left
# | *   * |     both

# Define the bands.
edge = f"+-------+"
blank = f"|       |"
right = f"|     * |"
left = f"| *     |"
middle = f"|   *   |"
both = f"| *   * |"

# Define each die face.
one = [
    edge,
    blank,
    middle,
    blank,
    edge
]

two = [
    edge,
    right,
    blank,
    left,
    edge
]

three = [
    edge,
    right,
    middle,
    left,
    edge
]

four = [
    edge,
    both,
    blank,
    both,
    edge
]

five = [
    edge,
    both,
    middle,
    both,
    edge
]

six = [
    edge,
    both,
    both,
    both,
    edge
]

# Roll the dice!
qty = 1
if len(sys.argv) > 1:
    num = int(sys.argv[1])
    qty = num if num <=5 else 5

draw_dice([get_digit() for i in range(qty)])
