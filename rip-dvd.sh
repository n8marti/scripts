#!/bin/bash

# Use ddrescue to image DVD found at given device path.
# Use handbrake-cli to convert DVD image to mp4 with proper settings.
# Use IMDb to create proper destination folder and file names?
#   - API info: https://developer.imdb.com/documentation/api-documentation/getting-access
#   - This is possible, but overly complicated for a basic, personal use.
# Prerequisites:
#   - gddrescue
#   - handbrake-cli
#   - libdvd-pkg (with manual reconfigure)
#
# Other notes:
#   - rip between $start_sec and $stop_sec:
#       # Using dvdsimple://... without '#1' to denote title seems to bizarrely convert to 24fps output.
#       $ vlc -v dvd:///path/to/source.img#1 \
#           --audio-language=eng --start-time=$start_sec --stop-time=$stop_sec \
#           --sout='#transcode{vcodec=h264,vb=2048,deinterlace,acodec=mp4a,ab=160,channels=2,scodec=subt}:standard{access=file,mux=mp4,dst="out.mp4"}' vlc://quit
#   - concat MP4 files:
#       # https://stackoverflow.com/questions/7333232/how-to-concatenate-two-mp4-files-using-ffmpeg#11175851
#       $ cat files.list
#       file 'file1.mp4'
#       file 'file2.mp4'
#       # No transcoding.
#       $ ffmpeg -ignore_unknown -f concat -safe 0 -i files.list -map 0 -c copy merged.mp4
#       # OR
#       # Full re-transcoding.
#       $ ffmpeg -i file1.mp4 -i file2.mp4 -filter_complex \
#           '[0:0]scale=720:480:force_original_aspect_ratio=decrease,pad=720:480[v0]; \
#           [1:2]scale=720:480:force_original_aspect_ratio=decrease,pad=720:480[v1]; \
#           [v0][0:a][v1][1:a]concat=n=2:v=1:a=1[v][a]' \
#           -map '[v]' -map '[a]' merged.mp4
#       # Identical file types.
#       $ vlc -I dummy file1.mp4 file2.mp4 vlc://quit --sout-file-append --sout='file/ts:merged.mp4'

# Set global variables.
IMAGE=yes
KEEP_FILES=
NO_DVDNAV=
RIP=yes
SRC_DEV=/dev/sr0
SRC_RIP=
VERBOSE=

show_usage() {
    echo "Usage:"
    echo "  $0 [-hb]"
    echo "  $0 [OPT...] [/PATH/TO/FILE.MP4]"
    echo "  $0 [OPT...] -k /MOUNT/POINT \"TITLE (YEAR)\""
}

show_help() {
    show_usage
    echo "
Create DVD image, then rip to MP4 file to given /PATH/TO/FILE.MP4 or
/PATH/TO/DIR. If a path is not given, save files to user's home folder.

  -b    Attempt to decrypt the DVD's CSS and exit.
  -d    Use set -x for debug output.
  -h    Show this help and exit.
  -r    Rip video straight from disc without trying to image it.
  -R    Rip video from image regardless of rescue state.
  -s    Save (don't delete) disc recovery image and map file.

  -k /MOUNT/POINT \"TITLE (YEAR)\"
        Kodi-style outfile at /MOUNT/POINT/Videos/TITLE (YEAR)/TITLE (YEAR).mp4."
}

ensure_elevated_privileges() {
    # Ensure elevated privileges.
    if [ "$(id -u)" -ne 0 ]; then
        echo "ERROR: Please run script again with root privileges."
        exit 1
    fi
}

decrypt_dvd() {
    echo "Decrypting DVD..."
    if [[ -n "$VERBOSE" ]]; then
        HandBrakeCLI --input "$SRC_DEV" --title 0
    else
        HandBrakeCLI --input "$SRC_DEV" --title 0 2>/dev/null
    fi
    if [[ $? -ne 0 ]]; then
        echo "Unable to decrypt DVD. Exiting."
        exit 1
    fi
}

rip_dvd() {
    touch "$DST_MP4"
    chown 1000:1000 "$DST_MP4"
    chmod 777 "$DST_MP4"

    if [[ -n "$VERBOSE" ]]; then
        # HandBrakeCLI --input "$DST_IMG" --main-feature --output "$DST_MP4" --markers --align-av --encoder "x264" --encoder-preset "fast" --encoder-profile "main" --encoder-level '4.0' --quality "22" --two-pass --turbo --rate "30" --pfr --audio-lang-list "eng,fre" --first-audio --audio-copy-mask "copy:aac" --audio-fallback "av_aac" --ab "160" --ac '-1' --mixdown "stereo" --maxHeight "1080" --decomb --subtitle-lang-list "eng,fre" --first-subtitle --subtitle-default "none" --native-language "eng" --native-dub
        HandBrakeCLI $NO_DVDNAV --input "$SRC_RIP" --main-feature \
            --output "$DST_MP4" --markers --align-av \
            --encoder "x264" \
                --encoder-preset "fast" \
                --encoder-profile "main" \
                --encoder-level '4.0' \
                --quality "22" \
                --two-pass \
                --turbo \
                --rate "30" \
                --pfr \
            --audio-lang-list "eng,fre" \
                --first-audio \
                --audio-copy-mask "copy:aac" \
                --audio-fallback "av_aac" \
                --ab "160" \
                --ac '-1' \
                --mixdown "stereo" \
            --maxHeight "1080" \
                --decomb \
            --subtitle-lang-list "eng,fre" \
                --first-subtitle \
                --subtitle-default "none" \
                --native-language "eng" \
                --native-dub
    else
        HandBrakeCLI $NO_DVDNAV --input "$SRC_RIP" --main-feature \
            --output "$DST_MP4" --markers --align-av \
            --encoder "x264" \
                --encoder-preset "fast" \
                --encoder-profile "main" \
                --encoder-level '4.0' \
                --quality "22" \
                --two-pass \
                --turbo \
                --rate "30" \
                --pfr \
            --audio-lang-list "eng,fre" \
                --first-audio \
                --audio-copy-mask "copy:aac" \
                --audio-fallback "av_aac" \
                --ab "160" \
                --ac '-1' \
                --mixdown "stereo" \
            --maxHeight "1080" \
                --decomb \
            --subtitle-lang-list "eng,fre" \
                --first-subtitle \
                --subtitle-default "none" \
                --native-language "eng" \
                --native-dub \
                2>/dev/null
    fi
}

rescue_dvd() {
    echo "Preallocating image file; this could take a few minutes..."
    state='started'
    until ddrescuelog --done-status "${DST_MAP}" 2>/dev/null; do
        # Ensure outfiles.
        touch "$DST_IMG" "$DST_MAP"
        chown 1000:1000 "$DST_IMG" "$DST_MAP"
        chmod 777 "$DST_IMG" "$DST_MAP"

        case "$state" in
            'started')
                # Run initial ddrescue command.
                run_ddrescue --no-scrape
                # Update state.
                if [[ $? -eq 0 ]]; then
                    state='trimmed'
                fi
                ;;
            'trimmed')
                # NOTE: Omitting scraping phase because its results aren't worth the time.
                #   Each I/O timeout takes ~54 sec, and it happens for every bad block of 2 KB.
                KEEP_FILES=yes # allow further inspection of IMG and MAP files
                NO_DVDNAV=--no-dvdnav # avoid problems with bad navigation data
                ask_continue "Incomplete image. Continue ripping anyway?"
                break
                ;;
        esac
        echo
    done
}

ask_continue() {
    question="$1"
    read -rp "${question} [Y/n]: " response
    if [[ "${response,,}" == 'n' ]]; then
        exit 1
    fi
}

run_ddrescue() {
    # The RPi4 seems to drop the /dev/sr0 device if read rate exceeds 7 MB/s.
    # VERBOSE=
    # if [[ -n "$DEBUG" ]]; then
    #     VERBOSE=--verbose
        # ddrescue "$@" --sector-size=2048 --idirect --preallocate --max-read-rate=7M --verbose "$SRC_DEV" "$DST_IMG" "$DST_MAP"
    # else
        # ddrescue "$@" --sector-size=2048 --idirect --preallocate --max-read-rate=7M "$SRC_DEV" "$DST_IMG" "$DST_MAP"
    # fi
    ddrescue "$@" --sector-size=2048 --idirect --preallocate --max-read-rate=7M $VERBOSE "$SRC_DEV" "$DST_IMG" "$DST_MAP"
}


# Parse arguments.
while getopts ":bhk:nrRsv" o; do
    case "${o}" in
        b) # break
            VERBOSE=--verbose
            decrypt_dvd
            exit $?
            ;;
        h) # help
            show_help
            exit 0
            ;;
        k) # Kodi
            EXT_DRV="$OPTARG"
            ;;
        n)
            NO_DVDNAV=--no-dvdnav
            ;;
        r) # rip from DVD
            SRC_RIP="$SRC_DEV"
            IMAGE=
            ;;
        R) # rip from image
            SRC_DEV=
            ;;
        s) # save
            KEEP_FILES=yes
            ;;
        v) # verbose
            set -x
            VERBOSE=--verbose
            ;;
        *) # other
            show_usage
            exit 1
            ;;
    esac
done
shift $((OPTIND-1))

# Set remaining globals.
sec_start=$(date +%s)
# Set DST_DIR.
if [[ -d "$1" ]]; then
    DST_DIR="$1"
elif [[ "${1##*.}" == 'mp4' ]]; then
    DST_DIR="$(dirname "$1")"
    filename="$(basename "$1")"
    NAME="${filename%.*}"
    DST_MP4="$1"
elif [[ -d "$EXT_DRV" ]]; then
    NAME="$1"
    DST_DIR="${EXT_DRV}/Videos/${NAME}"
    DST_MP4="${DST_DIR}/${NAME}.mp4"
else
    if [[ -n "$SUDO_USER" ]]; then
        DST_DIR="/home/${SUDO_USER}"
    else
        DST_DIR="$HOME"
    fi
fi
# Set disc name.
if [[ -z "$NAME" ]]; then
    NAME="$(lsblk --output=LABEL --noheadings $SRC_DEV)"
fi
# Set MP4 filename.
if [[ -z "$DST_MP4" ]]; then
    DST_MP4="${DST_DIR}/${NAME}.mp4"
fi
# Set ddrescue filenames.
DST_IMG="${DST_DIR}/${NAME}.img"
DST_MAP="${DST_DIR}/${NAME}.map"
# Set rip source.
if [[ -z "$SRC_RIP" ]]; then
    SRC_RIP="$DST_IMG"
fi

# Create disc image with ddrescue.
# TODO: Verify at least 8.5 GB of free space on $dst disk. (Using preallocate for now.)
# Ensure DVD is unmounted.
if [[ -n "$SRC_DEV" && $(mount | grep $SRC_DEV >/dev/null) -eq 0 ]]; then
    ensure_elevated_privileges
    umount $SRC_DEV
fi

# Decrypt DVD for imaging, if needed.
#   NOTE: Decryption info is stored by libdvdcss in $HOME/.dvdcss/.
mkdir --parents --mode=777 "$DST_DIR"
chown 1000:1000 "$DST_DIR"
if [[ ! -e "$DST_IMG" && -n "$IMAGE" ]]; then
    ensure_elevated_privileges
    decrypt_dvd
    sec_decrypt=$(date +%s)
    echo "Disc decrypted after $(date -u -d@$((sec_decrypt - sec_start)) +%Hh%Mm%Ss)."
    echo
fi

if [[ -n "$IMAGE" ]]; then
    if [[ -n "$SRC_DEV" ]]; then
        ensure_elevated_privileges
        # Run ddrescue, initially assuming $src will be problem-free.
        rescue_dvd
        sec_rescue=$(date +%s)
        echo "Disc rescued after $(date -u -d@$((sec_rescue - sec_start)) +%Hh%Mm%Ss)."
        eject "$SRC_DEV"
        echo
    fi

    # Verify that image exists and is readable.
    if [[ ! -r "$DST_IMG" ]]; then
        echo "ERROR: Image \"$DST_IMG\" does not exist or is not readable."
        exit 1
    fi
fi

if [[ -n "$RIP" ]]; then
    # Rip video from image file.
    rip_dvd
    rip_code=$?
    sec_rip=$(date +%s)
    echo "Video ripped after $(date -u -d@$((sec_rip - sec_start)) +%Hh%Mm%Ss)."
    # Eject disc if ripped straight from DVD.
    if [[ "$SRC_RIP" == "$SRC_DEV" ]]; then
        eject "$SRC_DEV" 2>/dev/null
    fi
    echo
fi

# Offer to remove recovery files.
if [[ $rip_code -eq 0 ]]; then
    # Remove disc image.
    if [[ ! $KEEP_FILES == 'yes' ]]; then
        rm -f "$DST_IMG" "$DST_MAP"
    fi
    echo "\"$DST_MP4\" is ready!"
else
    rm -f "$DST_MP4"
    echo "ERROR: Failed to rip DVD."
    exit 1
fi
