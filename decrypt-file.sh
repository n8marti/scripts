#!/bin/bash

# - APKs can be unzipped with:
#   unzip FILE.APK
# - But the files in the "resources" folder are encrypted. This was an attempt
#   at figuring out how to unencrypt them.

echo "This script is not effective."
exit


# Define functions.
decrypt_file() {
    local i="$1"
    local k="$2"
    local c="$3"
    if [[ "$c" == 'default' ]]; then
        openssl enc -d -base64 -in "$i" -out "$(get_outfile_name "$i")" -K "$(cat "$k")" # -pass file:"$k"
        local ec="$?"
    else
        openssl enc -d -base64 -in "$i" -out "$(get_outfile_name "$i")" -K "$(cat "$k")" "$c" 2>/dev/null # -pass file:"$k" "$c" 2>/dev/null
        local ec="$?"
    fi
    return "$ec"
}

get_outfile_name() {
    local i="$1"
    echo "${i}.dec"
}

list_text_files() {
    local d="$1"
    for f in $(ls -1 "$d"); do
        f=$(realpath "${d}/${f}")
        type=$(file "$f" | cut -d':' -f2)
        if [[ $(echo "$type" | awk '{print $1}') == 'ASCII' ]]; then
            echo "$f"
        fi
    done
}

exit_success() {
    local i="$1"
    local k="$2"
    local c="$3"
    echo "Decryption successful for $i"
    echo -e "  keyfile:\t$k"
    echo -e "  cipher:\t$c"
    echo -e "  outfile:\t$(get_outfile_name "$i")"
    exit 0
}

# Handle options.
usage="Usage: $0 INPUT_FILE"
if [[ -z "$1" || "$1" == '-h' ]]; then
    echo "$usage"
    exit 1
elif [[ ! -r "$1" ]]; then
    echo "File not readable: $1"
    exit 1
fi

# Define variables.
in_file=$(realpath "$1")
assets_dir=$(dirname "$in_file")
apk_dir=$(dirname "$assets_dir")

# List possible key files.
echo "Searching for possible key files in ${assets_dir}..."
text_files=$(list_text_files "$assets_dir")

# Try decrypting without cipher. Use any found text files as potential key files.
echo "Attempting to decrypt with default cipher..."
for k in $text_files; do # assumes no spaces in filenames
    decrypt_file "$in_file" "$k" "default"
    ec="$?"
    diffd=$(diff "$in_file" $(get_outfile_name "$in_file"))
    if [[ -s $(get_outfile_name "$in_file") && -n "$diffd" ]]; then
        exit_success "$in_file" "$k" "default"
    fi
done

# If that fails, try decrypting with all available ciphers.
echo "Attempting to decrypt with all available ciphers..."
for k in $text_files; do # assumes no spaces in filenames
    for c in $(LANG=C openssl enc -list | grep -v Supported); do
        decrypt_file "$in_file" "$k" "$c"
        ec="$?"
        diffd=$(diff "$in_file" $(get_outfile_name "$in_file"))
        if [[ "$ec" -eq 0 && -n "$diffd" ]]; then
            exit_success "$in_file" "$k" "$c"
        fi
    done
done
echo "File not decrypted: $in_file"
rm -f "$(get_outfile_name "$in_file")"
exit 0


# Try other options if 1st attempt failed.
if [[ "$ec" -ne 0 || -z $(diff "$in_file" "$out_file") ]]; then
    for c in $(LANG=C openssl enc -list | grep -v Supported); do
        echo " - trying ${c#-}..."
        openssl enc -d -in "$in_file" -out "$out_file" -pass file:"$key_file" "$c" 2>/dev/null
        if [[ $? -eq 0 ]]; then
            exit 0
        fi
    done
    echo "Unable to decrypt file."
    rm "$out_file"
    exit 1
fi
