#!/bin/bash

# List n largest files on the system.

# Show help.
usage="Usage: $0 [BASE_DIR [QTY]]"
help="
  BASE_DIR  Examine all files recursively in BASE_DIR. [default: \"/\"]
  QTY       Number of files to list. [default: 10]

NOTE: The script requires BASE_DIR to be passed if QTY is given.

Examples:
  # Show 10 largest files on /.
  $0

  # Show 10 largest files on USB_DRIVE.
  $0 /media/\$USER/USB_DRIVE

  # Show 3 largest files on USB_DRIVE.
  $0 /media/\$USER/USB_DRIVE 3

  # Error (no BASE_DIR given before QTY).
  $0 20"

if [[ "$1" == '-h' || "$1" == '--help' ]]; then
    echo "$usage"
    echo "$help"
    exit 0
fi

# Set base folder to search.
dir='/'
if [[ -d "$1" ]]; then
    dir="$1"
fi
if [[ ! -d "$dir" ]]; then
    echo "$usage"
    exit 1
fi

# Set the number of files listed.
n="$2"
if [[ -z "$n" ]]; then
    n=10
fi

# Show largest files and their sizes in bytes.
fs=$(df --output=source "$dir" | tail -n1)
echo "Finding top $n files by size (B) in $dir on ${fs}..."
find "$dir" -xdev -type f -printf '%s %p\n' 2>/dev/null | sort -nr | head -n "$n"
