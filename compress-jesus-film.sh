#!/bin/bash

# This aims to create a video with the same properties as jesusfilm.org's "low"
#   quality download option from any higher quality file.

height=240
usage="Usage: $0 [-p PIXEL_HEIGHT] INFILE"
while getopts "hp:" o; do
    case "$o" in
        h)
            echo "$usage"
            exit 0
            ;;
        p)
            height="$OPTARG"
            ;;
        *)
            echo "$usage"
            exit 1
    esac
done
shift $((OPTIND - 1))

ab=64000
if [[ "$height" == "720" ]]; then
    ab=128000
fi

ffmpeg -i "$1" -filter_complex "[0:v]scale=trunc(oh*a/2)*2:${height}[s0];[s0]fps=23[s1]" -map [s1] -map 0:a -f mp4 -profile:v baseline -b:v 149668 -b:a "$ab" -acodec aac -bufsize 74834 -loglevel warning -maxrate 149668 -vcodec libx264 -stats "${1%.*}-${height}p.mp4"
