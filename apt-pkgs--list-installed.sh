#!/bin/bash

# - List all the packages installed on the system.

apt list --installed | grep '/' | cut -d '/' -f1
