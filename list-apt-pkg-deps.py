#!/usr/bin/env python3

import re
import subprocess
import sys

apt_pkg = sys.argv[1]
deps = set()
cmd = ['apt-rdepends', apt_pkg]
p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.DEVNULL, encoding='UTF8')
with p.stdout:
    for line in iter(p.stdout.readline, ''):
        if line[0] != ' ':
            deps.add(line.rstrip())
        elif 'Depends' in line:
            # print(line.rstrip())
            depends = line.split(':')[1].strip()
            # print(depends)
            pkg = depends.split()[0].strip()
            deps.add(pkg)

deps.remove(apt_pkg)
deps = sorted(list(deps))
for dep in deps:
    print(dep)
