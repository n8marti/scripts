""" Snapd bash commands recreated for Python using the Core REST API """

# Built on example shared by SO user david-k-hess:
# https://stackoverflow.com/a/59594889

import requests
import socket
import sys

from urllib3.connection import HTTPConnection
from urllib3.connectionpool import HTTPConnectionPool
from requests.adapters import HTTPAdapter


class SnapdConnection(HTTPConnection):
    def __init__(self):
        super().__init__("localhost")

    def connect(self):
        self.sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
        self.sock.connect("/run/snapd.socket")

class SnapdConnectionPool(HTTPConnectionPool):
    def __init__(self):
        super().__init__("localhost")

    def _new_conn(self):
        return SnapdConnection()

class SnapdAdapter(HTTPAdapter):
    def get_connection(self, url, proxies=None):
        return SnapdConnectionPool()

class Snap():
    def __init__(self):
        self.session = requests.Session()
        self.fake_http = 'http://snapd/'
        self.session.mount(self.fake_http, SnapdAdapter())

    def list(self):
        payload = '/v2/snaps'
        result = self.session.get(self.fake_http + payload).json()['result']
        return result

    def info(self, snap):
        payload = '/v2/snaps/' + snap
        result = self.session.get(self.fake_http + payload).json()['result']
        return result

    def refresh_list(self):
        payload = '/v2/find?select=refresh'
        result = self.session.get(self.fake_http + payload).json()['result']
        if type(result) is dict:
            print(result['message'])
            result = []
        return result

    def system_info(self):
        payload = '/v2/system-info'
        result = self.session.get(self.fake_http + payload).json()['result']
        return result


snap = Snap()
args = sys.argv
relpath = args[0] if len(args) > 0 else ''
cmd = args[1] if len(args) > 1 else ''
cmdarg = args[2] if len(args) > 2 else ''

cmds = ['list', 'info', 'refresh_list', 'system_info']
if cmd == '--help':
    print('Command options:')
    print(*cmds, sep='\n')
elif cmd == 'list':
    print(snap.list())
elif cmd == 'info':
    print(snap.info(cmdarg))
elif cmd == 'refresh_list':
    updatables = snap.refresh_list()
    for i in updatables:
        print('{}: {} B'.format(i['name'], i['download-size']))
elif cmd == 'system_info':
    print(snap.system_info())
