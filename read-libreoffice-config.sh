#!/bin/bash

cfg_file="$1"

# Get listing of all unique property paths
pa_pat='s|^<item oor:path="/org[.]openoffice[.]([^>]*)>.*$|"\1|'
all_paths=$(cat $cfg_file | sed -r "$pa_pat" | sort -u)

show_category() {
    ct=0
    echo "\"$1\" settings:"
    while read -r line; do
        ((ct += 1 ))
        pa_pat='s|^<item oor:path="/org[.]openoffice[.]([^>]*)>.*$|"\1|'
        path=$(echo $line | sed -r "$pa_pat")
        cct=0
        if [[ $path =~ $1 ]]; then
            ((cct += 1))
            pr_pat='s|.*prop oor:name=(".*") .*|\1|'
            prop=$(echo $line | sed -r "$pr_pat")

            v_pat='s|.*<value>(.*)</value>.*|\1|'
            value=$(echo $line | sed -r "$v_pat")
            echo -e "\t$ct: $prop = $value"
        fi
    done < "$cfg_file"
    echo
    }

ask_line() {
    read -p "Line # to show: " line
    show_line $line
    }

show_line() {
    if [[ $line =~ [0-9]* ]]; then
        cat "$cfg_file" | head -$1 | tail -1
    else
        exit 0
    fi
    }

categories=(
    History
    Office.Calc
    Office.Common
    Office.DataAccess
    Office.Linguistic
    Office.Logging
    Office.Recovery
    Office.UI
    Office.Views
    Office.Writer
    Office.WriterWeb
    Setup
    UserProfile
    ucb.
    )

options=(
    Help
    Quit
    All
    Show-line
    ${categories[@]}
    )

PS3="
Please choose a category # [or 1) Help or 2) Quit]: "

echo
select option in ${options[@]}; do
    echo
    case $option in
        Quit)
            exit 0
            ;;
        Show-line)
            ask_line
            ;;
        Help)
            n=0
            for o in ${options[@]}; do
                ((n += 1))
                echo "$n) $o"
            done
            ;;
        All)
            for c in ${categories[@]}; do
                show_category $c
            done
            ask_line
            ;;
        *)
            show_category $option
            ask_line
            ;;
    esac
done

exit 0
