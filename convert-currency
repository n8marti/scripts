#!/bin/bash

# Validate args.
help="Usage: $0 AMT CUR"
if [[ $1 == '-h' ]] || [[ $1 == '--help' ]]; then
    echo "$help"
    exit 0
elif [[ $2 != 'USD' ]] && [[ $2 != 'XAF' ]]; then
    echo "$help"
    exit 1
fi

# Gather script args.
parent_dir=$(realpath $(dirname $0))
amount=$(echo "$1" | tr -d ',')
from="$2"

# Set initial variables.
of="${HOME}/rate-USD-to-XAF.txt"
usd_link="https://api.exchangerate-api.com/v4/latest/USD"
default='570'
when='historical average'

# Get the USD to EUR rate from given link.
USD_to_EUR=$(wget -qO- "${usd_link}" | jq '.rates.EUR' 2>/dev/null)
if [[ $USD_to_EUR == '' ]]; then
    if [[ -r $of ]]; then
        default=$(cat "$of" | cut -d' ' -f1)
        when=$(cat "$of" | cut -d' ' -f2)
    fi
    USD_to_XAF="$default"
    pred="(Offline, using 1 USD = $USD_to_XAF XAF from $when)"
else
    # Convert from EUR to XAF or vice versa.
    XAF_to_EUR='655.957' # hard conversion
    USD_to_XAF=$(echo "scale=6; $USD_to_EUR * $XAF_to_EUR" | bc)
    echo "$USD_to_XAF "$(date +%F) > "${of}"
    pred=''
fi
XAF_to_USD=$(echo "scale=6; 1 / $USD_to_XAF" | bc)

# Determine direction of conversion.
if [[ $from == 'USD' ]]; then
    to='XAF'
    value=$(echo "$amount * $USD_to_XAF" | bc -l | sed 's/\./,/')
    converted=$(printf "%.0f" "$value" | sed 's/,/\./')
elif [[ $from == 'XAF' ]]; then
    to='USD'
    value=$(echo "$amount * $XAF_to_USD" | bc -l | sed 's/\./,/')
    converted=$(printf "%.2f" "$value" | sed 's/,/\./')
else
    echo -e "\tCurrency not understood. Try again."
    exit 1
fi

# Output result.
echo "$converted $to $pred"

exit 0
