#!/bin/bash

###########################################################################
# This script parses the package.xml file for Windows updates and compares
# Windows 10 64-bit update files listed there with the ones already
# downloaded. It then displays any updates that are missing.
###########################################################################

### Set initial variables #################################################
name="w100"
arch="x64"
lang="glb"
base="/home/acatbadmin/Partages/windows/wsusoffline"
cache_dir="$base/cache"
superseded_file="$base/exclude/ExcludeList-superseded.txt"

### Define functions ######################################################
check_for_download ()
    {
    ls -1 "$base/client/w100-x64/glb/" | grep "$1" >/dev/null 2>&1
    return $?
    }

check_for_superseded ()
    {
    grep "$1" "$superseded_file" >/dev/null 2>&1
    return $?
    }

### Main processing #######################################################
# Parse xml file to find valid updates
echo -n "Parsing xml file..."
raw_links=$(xmlstarlet tr "$base/xslt/ExtractDownloadLinks-${name}-${arch}-${lang}.xsl" \
    "${cache_dir}/package.xml")

# Parse raw_links variable to add updates to associative arrays
declare -A kbs_by_hash
declare -A dates_by_hash
for l in $raw_links; do
    fname=${l##*/}
    fname=${fname%.*} # drop extension
    p=$(echo ${l%/*}) # get parent path
    gp=$(echo ${p%/*}) # get grandparent path
    p=$(echo ${p##*/}) # remove all parents from parent path
    gp=$(echo ${gp##*/}) # remove all parents from grandparent path
    date=$(echo $l | cut -d'/' -f9-10 | tr / -)
    date="$gp/$p"
    kb=$(echo $fname | cut -d'-' -f2)
    hash=$(echo $fname | cut -d'_' -f2)
    dates_by_hash[$hash]="$date"
    kbs_by_hash[$hash]="$kb"
done
echo "done"
echo

# Compare valid updates with downloaded files
echo "The following updates have yet to be downloaded:"
output=$(
for i in ${!kbs_by_hash[@]}; do
    kb="${kbs_by_hash[$i]}"
    dt="${dates_by_hash[$i]}"
    if ! check_for_download "$kb" && ! check_for_superseded "$kb"; then
        echo "$kb from $dt"
    fi
done
)

# Echo results
if [[ -z "$output" ]]; then
    echo "[none found]"
else
    echo "$output" | sort -n -k1
fi


exit 0
