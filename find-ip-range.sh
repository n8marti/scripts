#!/bin/bash

site="$1"

declare -A dns_list
dns_list=(
    ["50.7.155.138"]="France"
    ["101.110.36.163"]="Japan"
    ["208.67.220.220"]="OpenDNS"
    ["8.8.8.8"]="Google"
    )

microsoft=(
    "update.microsoft.com"
    "windowsupdate.microsoft.com"
    "www.windowsupdate.com"
    "download.windowsupdate.com"
    "download.microsoft.com"
    )

if [[ $site == 'microsoft' ]] | [[ $site = 'MS' ]]; then
    site=${microsoft[@]}
fi

ranges=()
for s in ${site[@]}; do

    echo "Getting IP ranges for $s..."
    for d in ${!dns_list[@]}; do
        dig=$(dig +short "$s" "$d" | grep -Eo '[0-9.]{7,}')

        #echo "Getting IP ranges from $d (${dns_list[$d]})..."
        for a in $dig; do
            #whois $a | grep -o -e inetnum -e NetRange
            #output=$(whois $a | grep -Eo '[0-9.]{7,} - [0-9.]{7,}')
            whois $a | grep -Eo '[0-9.]{7,} - [0-9.]{7,}'
            #IFS=$'\n'
            #for r in ${output[@]}; do
            #    echo "$r"
            #    ranges+="$r"
            #done
            #unset IFS
        done
    done
    echo
done

exit
IFS=$'\n' sorted=($(echo ${ranges[*]} | uniq | sort))
unset IFS
echo ${sorted[@]}
#echo ${ranges[@]} | uniq | sort
