#!/usr/bin/env bash

echo "Try get-all-usb-files.py instead."
exit 0

srcdir="$1"
outdir=./temp

mkdir -p "$outdir"
find "$srcdir" -type f \( \
    -not -iname \*.exe \
    -a -not -iname \*.ini \
    -a -not -iname \*.dat \
    -a -not -iname \*.dll \
    -a -not -iname \*.lnk \
    -a -not -iname \*.rar \
    \) -exec cp -t "$outdir" '{}' +
