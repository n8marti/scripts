#!/bin/bash

ip4_pub=165.22.82.201
ip4_vpn=10.16.1.1
ip6_pub=2a03:b0c0:3:e0::387:6001
ip6_vpn=fded:f4ce:74ba:f54a::1

echo "PUBLIC"
echo "IPv4: $ip4_pub"
echo "IPv6: $ip6_pub"
echo
echo "VPN wgcar0"
echo "IPv4: $ip4_vpn"
echo "IPv6: $ip6_vpn"
