#!/bin/bash

# This script needs to be run as 'sudo'.
if [[ $(id -u) -ne 0 ]]; then
	echo "Launching with sudo." >&2
    sudo "${0}"
	exit 0
fi

svc_name="wasta-app-adjustments.service"
svc_file="/etc/systemd/system/${svc_name}"

# Ensure the service file.
cat <<EOF > "${svc_file}"
[Unit]
Description=Wasta desktop app adjustments
# Run after graphical login prompt.
After=graphical.target
# Run after console login prompt.
#After=multi-user.target

[Service]
Type=simple
RemainAfterExit=yes
ExecStart=/usr/share/wasta-core/scripts/app-adjustments.sh
TimeoutStartSec=0

[Install]
WantedBy=graphical.target
#WantedBy=multi-user.target
EOF

# Reload service files; run service at boot.
systemctl daemon-reload
systemctl enable "$svc_name"
