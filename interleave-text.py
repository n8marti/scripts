#!/bin/env python3

# Alternate lines from two text files. Potentially useful for Paratext EAB/SAB files.

import sys

# Define input files.
file1 = sys.argv[1]
file2 = sys.argv[2]

# Read both files into memory (not idea if files are very large).
with open(file1) as f:
    file1_lines = f.readlines()
with open(file2) as f:
    file2_lines = f.readlines()

# Ensure that files have equal numbers of lines.
if len(file1_lines) != len(file2_lines):
    print(f"Error: Input files have unequal numbers of lines.")
    print(f"  {len(file1_lines)}  {file1}")
    print(f"  {len(file2_lines)}  {file2}")
    exit(1)

# Create list of alternating lines.
interleaved_lines = [None]*(len(file1_lines) + len(file2_lines)) # create empty list
interleaved_lines[::2] = file1_lines                             # add file1's lines to every even index
interleaved_lines[1::2] = file2_lines                            # add file2's lines to every odd index

# Print lines to stdout.
for l in interleaved_lines:
    print(l)
