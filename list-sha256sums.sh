#!/bin/bash

# Set target_dir if given.
if [[ "$1" == '-h' || "$1" == '--help' ]]; then
    echo "Usage: $0 [DIR] [OUTFILENAME]"
    echo "List sha256sums for all non-TXT files in a given directory. [default is $PWD]"
    exit 0
elif [[ -n "$1" && -d "$1" ]]; then
    target_dir="$1"
elif [[ -n "$1" && ! -d "$1" ]]; then
    echo "Error: \"$1\" is not a valid directory."
    exit 1
else
    target_dir="$PWD"
fi

# Set outfile if name given.
if [[ -n "$2" ]]; then
    base_name="$(basename "$(realpath $2)")"
    outbase="${base_name%.*}" # remove file extension, if given
    outfile="${outbase}.txt"
fi
# Need to be location agnostic, so cd to $target_dir first.
cd "$target_dir"
if [[ -n "$outfile" ]]; then
    find . -maxdepth 1 -type f -not -name '*.txt' -exec sha256sum {} \; | sort > "$outfile"
else
    find . -maxdepth 1 -type f -not -name '*.txt' -exec sha256sum {} \; | sort
fi
