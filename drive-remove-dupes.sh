#!/bin/bash

#drive-remove-dups-sh

# 1. Find all files with (2) in the name
# 2. Find all files with the same titles as those, but without the (2) (originals)
# 3. Delete all files from list 1 than have a corresponding file in list 2.
# OR Make a list of all files & their paths from list 1 that appear in list 2.

# List all files in Drive
drive_path="/home/nate/Documents/SIL Google Drive/"
#drive_path="/home/nate/Documents/SIL Google Drive/Wycliffe Stuff/Statements/"
file_list=$(find "$drive_path")
#echo "$file_list"

dupes_folder=/home/nate/drive-dupes

# All files to be removed will be listed in this temp file
#tmp2="/tmp/drive-list-2"
#both_files=()


# All folders and files with " (2)" in the name will be listed in this temp file
tmp1="/tmp/folders"
touch $tmp1 && truncate -s 0 $tmp1

tmp2="/tmp/files"
touch $tmp2 && truncate -s 0 $tmp2

tmp3="/tmp/compare"
touch $tmp3 && truncate -s 0 $tmp3

# Find all files in Drive folder with (2) in the name
#ls -R "$drive_path" | grep -E '\s\([0-9]\)$' >> $tmp1
find "$drive_path" -type d -regextype "egrep" -regex '.*\s\([0-9]\)$' >> $tmp1
find "$drive_path" -type f -regextype "egrep" -regex '.*\s\([0-9]\).*$' >> $tmp2
#ls -R "$drive_path" | grep -E '\s\([0-9]\).+' >> $tmp1
#gedit $tmp1

# Find all potential duplicate folders & compare them with the original.
while read line; do
	#name=$(echo "$line" | tr -d "([:digit:])")
	name=$(echo "$line" | sed -r 's@\s\([0-9]\)@@')

	# find folders with both this name and the same name appended by " ([0-9])" & remove any that are empty
	if [[ $line != "$name" ]]; then
		echo "$name" >> $tmp3
		echo "$line" >> $tmp3
		echo >> $tmp3
		rm -dv "$line"
		rm -dv "$name"
		# What about duplicate folders one isn't empty?
	fi
done < "$tmp1"
#gedit $tmp3


# Find all files in Drive folder with same name as list 1, but without (2).
while read dupe; do
	#name=$(echo "$line" | tr -d "([:digit:])")
	orig=$(echo "$dupe" | sed -r 's@\s\([0-9]\)@@')

	# find folders with both this name and the same name appended by " ([0-9])" & remove any that are empty
	if [[ $dupe != "$orig" ]]; then
		echo "$orig" >> $tmp3
		echo "$dupe" >> $tmp3

		# Check if original file exists
		ls -l "$orig" >/dev/null 2>&1
		code=$?
		if [[ $code == 2 ]]; then
			# No original. Duplicate needs to be renamed to original.
			#echo "renaming $dupe to $orig"
			mv -n "$dupe" "$orig"
		elif [[ $code == 0 ]]; then
			# Original exists. Need to compare original and duplicate
			echo "At least 2 copies of file exist. Need to compare them." >> $tmp3

			# Check to see file sizes are identical.
			orig_s=$(ls -s "$orig" | cut -d' ' -f1)
			dupe_s=$(ls -s "$dupe" | cut -d' ' -f1)
			size_match=1
			if [[ "$orig_s" != "$dupe_s" ]]; then
				echo "	> File sizes are different." >> $tmp3
				echo "		orig: $orig_s | dupe: $dupe_s"
				size_match=0
			fi

			# Check timestamps. (last modified?)
			orig_m=$(ls -l --time-style='+%s' "$orig" | cut --output-delimiter='-' -d' ' -f6)
			dupe_m=$(ls -l --time-style='+%s' "$dupe" | cut --output-delimiter='-' -d' ' -f6)
			time_match=1
			if [[ "$orig_m" != "$dupe_m" ]]; then
				echo "	> File modification times are different." >> $tmp3
				echo "		orig: $orig_m | dupe: $dupe_m"
				time_match=0
			fi
			
			# If size_match & time_match both equal "1", then assume files are identical. Keep file without " (2)".
			if [[ "$size_match" == 1 ]] && [[ "$time_match" == 1 ]]; then
				echo "Duplicate found. Moving file out of Drive to $dupes_folder folder:"
				echo "$dupe"
				echo
				mkdir -p "$dupes_folder"
				mv "$dupe" "$dupes_folder"
			fi
		fi
		echo >> $tmp3
	fi
done < "$tmp2"

gedit $tmp3
exit 0
