#!/usr/bin/python3

# - Takes 2 files as inputs, each of which contains one package per line.
# - The packages in the shorter list are subtracted from the longer list.
# - A new list is output to stdout.

import sys

def debug_msg(message):
    if debug:
        print(message)

def msg(message):
    if not debug:
        print(message)

script_relpath = sys.argv[0]
opts = sys.argv[1:]
debug = False
strip = True

# Check for debug argument.
if "-h" in opts or "--help" in opts or "help" in opts:
    print(f"Usage: {sys.argv[0]} [debug] FILE1 FILE2")
    print("\nFILE1 and FILE2 are assumed to be new-line separated lists of items. The two files are compared, and items that exist in the longer list but not in the shorter list are printed to stdout.\nNOTE: Any items in the shorter list but not in the longer list are printed to stdout, but if \"debug\" is used the number of these items that exist will be printed to stdout.")
    exit()
elif "debug" in opts:
    debug = True
    opts.remove("debug")
elif "no-strip" in opts:
    strip = False


# Verify passed arguments.
if len(opts) < 2:
    print("Not enough arguments")
    exit(1)
elif len(opts) > 2:
    print(f"Too many arguments; only comparing these two lists:\n{opts[0]}\n{opts[1]}")

# Slurp lists from input files.
infiles = opts[0:2]
with open(infiles[0], 'r') as f1:
    lines1 = f1.readlines()
    if strip:
        lines1 = [l.rstrip() for l in lines1]
with open(infiles[1], 'r') as f2:
    lines2 = f2.readlines()
    if strip:
        lines2 = [l.rstrip() for l in lines2]

# Compare list sizes and name accordingly.
if len(lines1) > len(lines2):
    big_list = lines1
    big_list_file = infiles[0]
    sm_list = lines2
    sm_list_file = infiles[1]
else:
    big_list = lines2
    big_list_file = infiles[1]
    sm_list = lines1
    sm_list_file = infiles[0]

# Subtract lists and output difference.
diffs = [i for i in big_list if i not in sm_list]
check = [i for i in sm_list if i not in big_list]

debug_msg(f"{len(diffs)} packages kept from {big_list_file}")
debug_msg(f"{len(check)} packages dropped from {sm_list_file}")
msg('\n'.join(diffs))
