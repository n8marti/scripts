#!/bin/bash

### Resize a virtual disk for a VirtualBox VM

# Get default folder.
default=$(grep defaultMachineFolder $HOME/.config/VirtualBox/VirtualBox.xml | awk -F'=' '{print $2}' | sed -r 's@"(.*)"\sdefaultHardDiskFormat@\1@')
start=$HOME
if [[ -d $default ]]; then
    start="${default}"
fi

# Choose a hard disk file
hard_disk=$(zenity --file-selection --filename="${start}" --title="Choose virtual disk file...")
if [[ $? -ne 0 ]]; then
    exit 1
fi

echo
echo "You selected:"
echo "$hard_disk"

# Get current disk size
VBoxManage showmediuminfo "$hard_disk" | grep 'Capacity:'

# Pick new size
echo
read -p "Enter new capacity in MB, greater than the current capacity:
" new_size

# Resize disk
echo
echo "Resizing now:"
VBoxManage modifyhd "$hard_disk" --resize "$new_size"

echo
echo "Hard disk has been resized to $new_size MB."
echo "You will now need to resize the partition, maybe by using GParted on an
Ubuntu iso, etc."

exit 0
