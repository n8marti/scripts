#!/bin/bash

# Use iconv to convert multiple UTF-8 w/BOM files to plain UTF-8.

for f in "$@"; do
    # echo -e "\nIn: $f"
    if [[ -z ${f##*.utf-8.txt} ]]; then
        # Arg "f" is already a converted file; i.e. ends in '.utf-8.txt'.
        # echo "$f is already converted."
        continue
    fi
    outfile=${f%*.txt}.utf-8.txt
    # echo "Out: $outfile"

    if [[ -e $outfile ]]; then
        echo "$outfile exists. Skipping conversion."
        continue
    fi
    iconv --from-code=UTF-16 --to-code=UTF-8 --output="$outfile" "$f"
done
