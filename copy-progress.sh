#!/bin/bash

echo "[script not ready]"
exit 0

#device=$(df "$SNAPS_DIR" | tail -n1 | sed -r 's|^/.*/([a-z]*)[0-9]*\s.*|\1|')

if [[ -n $1 ]]; then
    device="$1"
else
    device=$(df ~ | tail -n1 | tr -s ' ' | cut -d' ' -f1)
fi

sectors_written_initial=$(cat "/sys/block/$device/stat" | tr -s ' ' | cut -d ' ' -f7)
block_size=$(stat -c '%o' ~)
progress=0
while true; do
    #bytes_dest_current=$(($(du -s "$SNAPS_DIR" | cut -f1) * 1024))
    #bytes_copied=$(( $bytes_dest_current - $bytes_dest_initial ))
    sectors_written=$(cat "/sys/block/$device/stat" | tr -s ' ' | cut -d ' ' -f7)
    sectors_copied=$(( $sectors_written - $sectors_written_initial ))
    # Multiplying $sectors_copied by 4230 to get total bytes copied.
    #   This is based solely on observation--I can't logically explain it.
    #   This factor should be 512 according to documentation for /sys/block/<dev>/stat:
    #   https://www.kernel.org/doc/Documentation/block/stat.txt,
    #   but my observations say otherwise.
    if [[ $total_bytes_to_copy -gt 0 ]]; then
        progress=$(( $sectors_copied * $block_size * 100 / $total_bytes_to_copy ))
    else
        progress="100"
    fi

    #ec=$(tail -n1 "$tmp_copy" | cut -f1)
    #snap=$(tail -n1 "$tmp_copy" | cut -f2)

    #if [[ $ec -gt 0 ]]; then
    #    script_exit "$ec" "$COPY_PID"
    #fi

    echo "$progress"
    #echo "#$text $snap..."
    sleep 0.2
    # if
done
