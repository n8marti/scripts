#!/bin/bash

# Needs elevated privileges.
if [[ $(id -u) -ne 0 ]]; then
	echo "You must run this script with sudo."
	exit 1
fi

# Define variables.
port="443"
orig_dest="172.16.1.1"
new_dest="172.16.1.4"

# Allow IPv4 forwarding.
echo 1 >/proc/sys/net/ipv4/ip_forward

# Set routing rules.
iptables -F
iptables -t nat -F
iptables -X
iptables -t nat -A PREROUTING -p tcp --dport "$port" -j DNAT --to-destination "${orig_dest}:${port}"
iptables -t nat -A POSTROUTING -p tcp -d 172.16.1.1 --dport "$port" -j SNAT --to-source "$new_dest"
