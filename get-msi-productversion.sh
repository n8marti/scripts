#!/bin/bash

# Get Logos version from Logos-x64.msi file.

# Ensure msi file is passed as arg.
if [[ ! -n "$1" ]]; then
    echo "USAGE: $0 FILE.msi"
    exit 1
fi

# Ensure msiinfo is installed.
if [[ ! $(which msiinfo) ]]; then
    sudo apt-get install --yes msitools
fi

# Ensure Property table is present.
if [[ ! $(msiinfo tables "$1" | grep Property) ]]; then
    echo "No \"Property\" table found. List all tables with:"
    echo "msiinfo tables $1"
    exit 1
fi

# Get ProductVersion.
version=$(msiinfo export "$1" Property | grep ProductVersion | awk '{print $2}')
if [[ -n "$version" ]]; then
    echo "$version"
    exit 0
else
    echo "Version not found."
    exit 1
fi
