#!/bin/bash

# Merge audio from multiple JESUS Film videos into a single video with multiple audio streams.

tsv_file="[none given]"
help_text="Usage: $0 [-d BASE_DIR -H] -t TSV_FILE OUTFILE"
while getopts "d:f:ht:v" opt; do
    case $opt in
        d)
            base_dir=$(realpath "$OPTARG")
            ;;
        f)
            fmt="$OPTARG"
            ;;
        h)
            echo -e "$help_text"
            exit 0
            ;;
        t)
            tsv_file=$(realpath "$OPTARG")
            ;;
        v)
            set -x
            ;;
        *) # invalid option
            echo -e "$help_text"
            exit 1
            ;;
    esac
done
shift $((OPTIND - 1))

if [[ -z "$1" ]]; then
    echo "Error: No outfile given."
    exit 1
fi
ext="${outfile##*.}"
if [[ "$ext" != "$outfile" ]]; then
    # Use format implied by outfile name.
    fmt=".${ext}"
elif [[ -z "$fmt" ]]; then
    # Default format if none given or implied.
    fmt=".mp4"
fi

# Set base dir.
if [[ -n "$base_dir" ]]; then
    if [[ ! -d "$base_dir" ]]; then
        echo "Error: Not a directory: $base_dir"
        exit 1
    fi
else
    base_dir=$(dirname "$tsv_file")
    if [[ ! -d "$base_dir" ]]; then
        echo "Error: Not a directory: $base_dir"
        exit 1
    fi
fi

# Confirm TSV file.
if [[ ! -f "$tsv_file" ]]; then
    echo "Error: TSV file not readable: $tsv_file"
    exit 1
fi

# Parse TSV file.
inputs=()
maps=()
idx=0
while read -r l; do
    # Skip empty or commented lines.
    if [[ $l =~ ^$ || $l =~ ^# ]]; then
        echo "Line skipped: \"$l\""
        continue
    fi

    # Skip header line.
    if [[ "$(echo "$l" | awk -F'\t' '{print $1}')" == "stream" ]]; then
        echo "Header line skipped: \"$l\""
        continue
    fi

    # Parse line; ignore lines with no input file.
    source="$(echo "$l" | awk -F'\t' '{print $4}')"
    if [[ -z "$source" ]]; then
        continue
    fi

    # Check source file.
    if [[ ! -f "${base_dir}/${source}" ]]; then
        echo "Error: File not found: ${base_dir}/${source}"
        exit 1
    fi

    # Add data to command args.
    s="a"
    if [[ $idx -eq 0 ]]; then
        s="v"
        if [[ -z "$outfile" ]]; then
            file=$(basename "$source")
            name="${file%.*}"
            res="${name##*-}"
            if [[ -z "$res" ]]; then
                echo "Error: Could not determine output resolution from file: $source"
                exit 1
            fi
            outfile="${1%.*}-${res}${fmt}"
        fi
    fi
    inputs+=( -i "${base_dir}/${source}" )
    maps+=( "-map" "${idx}:${s}:0" )

    # Increment the counter.
    ((idx+=1))

done < "$tsv_file"

# Add final command args.
ab="64k"
if [[ "$res" == "720p" ]]; then
    ab="128k"
fi

ffmpeg "${inputs[@]}" "${maps[@]}" "-b:a" "$ab" "$outfile"
ec=$?

echo "Update metadata with:"
echo "ffmpeg-update-metadata.sh -f $tsv_file $outfile"

exit "$ec"
