#!/bin/bash

if [[ -z $1 ]]; then
    echo "Getting git info for $PWD..."
    repo_root="$PWD"
else
    repo_root="$1"
fi

git_config="${repo_root}/.git/config"

url_line=$(grep url "$git_config")
if [[ $? -eq 0 ]]; then
    echo "$url_line" | cut -d '=' -f2 | cut -d ' ' -f2
else
    echo "Error: \"url\" not found in $git_config"
    exit 1
fi
