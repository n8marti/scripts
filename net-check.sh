#! /bin/bash

# This checks for an internet connection with a couple of options to choose from.


### Initial variables #########################################################
site="rfi.fr"
dns="208.67.222.222"

usage='usage: $ net-check.sh [-d|p], or -h for help.'
help_text="$usage

Check for a resolvable internet connection using either \"dig\" or \"ping\".

    -d  Use the following dig command to check for an internet connection:
        $ dig +short @$dns $site
        
    -h  Show this help text.
    
    -p  Use the following ping command to check for an internet connection:
        $ ping -c3 $site
"


### Functions #################################################################
ping_check() {
    ping -c3 $site >/dev/null 2>&1
    result=$?
    if [[ $result != 0 ]]; then
        echo "No resolvable connection via ping."
        return 1
    else
        echo "Connection is good (ping)."
        return 0
    fi
    }
    
dig_check() {
    dig +short @$dns $site >/dev/null 2>&1
    result=$?
    if [[ $result != 0 ]]; then
        echo "No resolvable connection via dig."
        return 1
    else
        echo "Connection is good (dig)."
        return 0
    fi
    }


### Option handling ###########################################################
while getopts ":dhp" opt; do
    case $opt in
        d) # use dig
            dig_check
            exit $?
            ;;
        h) # help text
            echo "$help_text"
            exit 0
            ;;
        p) # use ping
            ping_check
            exit $?
            ;;
        \?) # message for invalid option
            echo "$usage"
            exit 3
            ;;
    esac
done
shift $(($OPTIND - 1))


### Default function if no option given #######################################
dig_check
exit $?
