#!/bin/bash

# Demo script for quick conversion to and from binary.

remove_spaces() {
    echo "$1" | sed -E 's|\W*||g'
}

is_binary() {
    non_binary_chars=$(echo "$1" | tr -d '0' | tr -d '1')
    if [[ -z "$non_binary_chars" ]]; then
        return 0
    else
        return 1
    fi
}

if [[ -z "$1" || "$1" == '-h' || "$1" == '--help' ]]; then
    echo -e "Usage: $0 [--decode] INPUT\n\n\$ $0 'TEXT'\n$($0 TEXT)"
    echo -e "\n\$ $0 --decode '01010100 01000101 01011000 01010100'"
    $0 --decode '01010100 01000101 01011000 01010100'
    exit 1
elif [[ "$1" == '--decode' ]]; then
    if [[ -n "$2" ]]; then
        # Remove spaces.
        spaceless=$(remove_spaces "$2")
        # Verify as binary.
        is_binary "$spaceless"
        if [[ $? -eq 0 ]]; then
            echo "$spaceless" | perl -lpe '$_=pack"B*",$_'
        else
            echo "ERROR: Not a binary number: $2"
            exit 1
        fi
    else
        echo "ERROR: Nothing to decode."
        exit 1
    fi
else
    echo "$1" | perl -lpe '$_=unpack"B*"' | sed -E 's/([01]{8})/\1 /g'
fi
