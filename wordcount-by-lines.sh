#!/bin/bash

usage="Usage: $0 [FILE] [START LINE] [END LINE]"
if [[ $1 == '-h' ]];then
    echo "$usage"
fi

if [[ -z $1 ]]; then
    echo "$usage"
fi
file="$(realpath "$1")"

if [[ -z $2 ]]; then
    start=1
else
    start="$2"
fi

if [[ -z $3 ]]; then
    end="$(wc -l < "$file")"
else
    end="$3"
fi

echo -e "   lines    wds"
head -n"$end" "$file" | tail -n+"$start" | wc -lw