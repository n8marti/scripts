#!/bin/bash


# Set default variables.
av_stream_ct=2 # one video, one audio

# Handle options.
help_text="Usage: $0 [-n NO_OF_AVSTREAMS] FILE.MP4 FILE.MP4 [FILE...]"
while getopts ":hn:r:v" opt; do
    case "$opt" in
        h)
            echo "$help_text"
            echo "This script assumes all MP4s have the same resolution and the media streams are in the same order."
            echo "It will retranscode the files. To concat files with identical properties, use:"
            echo "$ cat files.list"
            echo "file infile1.mp4"
            echo "file infile2.mp4"
            echo "$ ffmpeg -ignore_unknown -f concat -safe 0 -i files.list -map 0 -c copy outfile.mp4"
            exit 0
            ;;
        n)
            av_stream_ct="$OPTARG"
            ;;
        r)
            resolution="$OPTARG"
            ;;
        v)
            verbose=1
            ;;
        *) # invalid option
            echo "$help_text"
            exit 1
            ;;
    esac
done
shift $((OPTIND - 1))

# TODO: Handle option to force resolution.
#   $ ffmpeg -i file1.mp4 -i file2.mp4 -filter_complex \
#       '[0:0]scale=720:480:force_original_aspect_ratio=decrease,pad=720:480[v0]; \
#       [1:2]scale=720:480:force_original_aspect_ratio=decrease,pad=720:480[v1]; \
#       [v0][0:a][v1][1:a]concat=n=2:v=1:a=1[v][a]' \
#       -map '[v]' -map '[a]' merged.mp4


# Handle arguments.
file_ct="$#"
files=( "$@" )

input_args=()
input_streams=()
output_streams=()
# Build input args.
f_ct="$file_ct"
for f in "${files[@]}"; do
    f_i=$((file_ct - f_ct))
    input_args+=( -i "$f" )
    s_ct="$av_stream_ct"
    while [[ "$s_ct" -gt 0 ]]; do
        s_i=$((av_stream_ct - s_ct))
        input_streams+=( "[${f_i}:${s_i}]" )
        ((s_ct -= 1))
    done
    ((f_ct -= 1))
done
# Build filter_complex string and output_streams.
s_ct="$av_stream_ct"
while [[ "$s_ct" -gt 0 ]]; do
    if [[ "${#output_streams[@]}" -eq 0 ]]; then
        output_streams+=( "[v]" )
    else
        output_streams+=( "[a$((av_stream_ct - s_ct))]" )
    fi
    ((s_ct -= 1))
done
out_streams_ct="${#output_streams[@]}"
filter_complex="${input_streams[*]} concat=n=${file_ct}:v=1:a=$((out_streams_ct - 1)) ${output_streams[*]}"

# Build map_args.
map_args=()
map_ct="${#output_streams[@]}"
while [[ "$map_ct" -gt 0 ]]; do
    map_args+=( -map "${output_streams[$((out_streams_ct - map_ct))]}" )
    ((map_ct -= 1))
done

# Run concat command.
if [[ "$verbose" -eq 1 ]]; then
    echo ffmpeg "${input_args[@]}" -filter_complex "$filter_complex" "${map_args[@]}" concat.mp4
fi
ffmpeg -hide_banner -y "${input_args[@]}" -filter_complex "$filter_complex" "${map_args[@]}" concat.mp4