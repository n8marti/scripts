#!/bin/bash

# Accept either FileID or browser link of Google Drive file (must be publicly
#   accessible) and print out direct download link.
#   https://drive.google.com/u/3/uc?id=${file_id}&export=download

# Example:
# https://drive.google.com/file/d/1JEfEZKXhtEEKCm8wBrSSPWXe0b01PhZu/view?usp=drive_open
# file_id is 6th term in link.
input="$1"

# Assume browser link first.
file_id=$(echo "$input" | awk -F'/' '{print $6}')
# If no result, assume file_id was passed.
if [[ -z "$file_id" ]]; then
    file_id="$input"
fi

# Return download link.
# TODO: This doesn't work for files too large for Drive to scan.
echo "https://drive.google.com/u/3/uc?id=${file_id}&export=download"
