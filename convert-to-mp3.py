#!/usr/bin/env python3

# Batch conversion to MP3 using ffmpeg. Also accepts single file names.

import subprocess
import sys

from pathlib import Path


def parse_args(args):
    usage = f"Usage: {args[0]} -h | /PATH/TO/FILE/OR/FOLDER"
    source = None
    for a in args[1:]:
        if a == '-h' or a == '--help':
            print(usage)
            exit()
        elif validate_input(a):
            source = validate_input(a)
        else:
            print(f"ERROR: Invalid option: {o}")
    if not source:
        print(usage)
        exit(1)
    return source

def validate_input(input_string):
    p = Path(input_string).resolve()
    if not p.exists():
        print(f"ERROR: Not a valid file or folder: {input_string}")
        exit(1)
    return p

def get_file_type(filepath):
    if filepath.is_file():
        return 'file'
    elif filepath.is_dir():
        return 'dir'

def get_file_list(dir):
    return [f for f in dir.iterdir() if f.suffix.lower() != '.mp3']

def convert_file_to_mp3(infile):
    outfile = infile.with_suffix('.mp3')
    print(f"Converting {outfile}...")
    if outfile.exists():
        clobber = False
        try:
            ans = input(f"Overwrite {outfile}? [y/N]: ").lower()
        except KeyboardInterrupt:
            print("\nUser cancelled with Ctrl+C.")
            return
        if ans == 'y' or ans == 'yes':
            clobber = True
        if not clobber:
            print(f"Skipped: {infile}")
            return
    cmd = ['ffmpeg', '-v', 'error', '-stats', '-y', '-i', infile, outfile]
    try:
        r = subprocess.run(cmd, stderr=subprocess.STDOUT)
    except KeyboardInterrupt:
        print(f"User cancelled with Ctrl+C. Removing file: {outfile}")
        outfile.unlink()
        return
    if r.returncode != 0:
        print(r.stderr)
        exit(1)

def main():
    source = parse_args(sys.argv)
    source_type = get_file_type(source)
    if source_type == 'dir':
        files = get_file_list(source)
    elif source_type == 'file':
        files = [source]
    for f in files:
        convert_file_to_mp3(f)


if __name__ == '__main__':
    main()
