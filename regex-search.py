#!/usr/bin/env python3

import regex
import sys
from pathlib import Path


def parse_args(args):
    patterns = {}
    text = None
    action = None

    if len(args) == 1:
        # No args passed.
        # print(f"Usage:\n  {args[0]} REGEXP [REPLACE] FILE\n  cat FILE | {args[0]} REGEXP")  # noqa: E501
        print(f"Usage: {args[0]} REGEXP [REPLACE] FILE")
        exit(1)
    else:
        patterns['search'] = rf"{args[1]}"
        if len(args) in [1, 2]:
            if args[1] == '-h' or args[1] == '--help':
                print(help(regex))
                exit()
            # Not yet able to handle input from stdin.
            print(f"Usage: {args[0]} REGEXP [REPLACE] FILE")
            exit(1)
            # Regex passed, but no file. Use stdin as text.
            text = sys.stdin.read()
            # Replace Windows CRLF with LF.
            # end = get_line_endings(text)
            end = None
            text = text.encode().replace(b'\r\n', b'\n').decode()
        elif len(args) > 2:
            # Regex and file passed. Use both, ignoring additional args.
            infile = Path(args[-1]).resolve()
            if len(args) > 3:
                patterns['replace'] = args[2]
                action = 'replace'
            else:
                action = 'search'
            if not infile.is_file():
                print(f"Error: File doesn't exist: {infile}")
                exit(1)

            text = infile.read_text()
            # end = get_line_endings(text)
            end = None
    return patterns, text, end, action


def search(pattern, text):
    results = regex.findall(pattern, text)
    for r in results:
        print(r)


def replace(patterns, text):
    results = regex.sub(patterns.get('search'), patterns.get('replace'), text)
    for line in results.splitlines():
        print(line)


def get_line_endings(text):
    # TODO: Doesn't work. Seems to automatically convert '\r\n' to '\n'.
    print(text[-2:].encode())
    last_two_hex = [f"{c.encode().hex()}" for c in text[-2:]]
    print(last_two_hex)
    if last_two_hex == ['\x0d', '\x0a']:
        end = 'CRLF'
    elif last_two_hex[-1] == '0a':
        end = 'LF'
    else:
        print(f"Error: Not a valid line ending: {last_two_hex[-1]}")
        exit(1)
    return end


def main():
    patterns, text, end, action = parse_args(sys.argv)
    # print(end)
    if action == 'search':
        search(patterns.get('search'), text)
    elif action == 'replace':
        replace(patterns, text)

    # for i in text:
    #     print(f"{i}\t{i.encode()}")
    # print(ord(text))
    # chars = [f"{ord(c):02x}" for c in text]
    # print(f"{' '.join(chars)}")
    # for c in text:
        # print(c.encode())
    # print(text)


if __name__ == '__main__':
    main()
