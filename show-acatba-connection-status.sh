#!/bin/bash

# Set variables.
LANG=
wg_if='wgcar0'
delay="30s"
if [[ "$1" ]]; then
    delay="$1"
fi

get_last_handshake() {
    sudo wg show "$wg_if" | grep -A1 "$1" | tail -n1 | awk -F':' '{print $2}'
}

get_last_handshake() {
    local epoch_s=$(sudo wg show "$wg_if" dump | grep "$1" | cut -f5)
    date --date="@${epoch_s}"
}

print_data() {
    local ip="$1"
    local last_hs=$(get_last_handshake "$ip")
    echo -e "$(date)  $ip:\t$last_hs"
}

print_data() {
    local ip="$1"
    local last_hs=$(get_last_handshake "$ip")
    echo -e "${ip} last seen ${last_hs}"
}

#tput sc
while true; do
    #tput ed
    echo $(date)
    for a in "10.16.1.4/32" "10.16.1.254/32"; do
        print_data "$a"
    done
    echo
    sleep "$delay"
    #tput rc
done
