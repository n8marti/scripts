#!/bin/bash

# Get useful stats from vcgencmd and present human-readable output.
# Stats:
#   - CPU temp.
#   - CPU freq.
#   - CPU throttling
#   - CPU voltage

# vcgencmd commands:
#	- ap_output_control
#	- ap_output_post_processing
#	- arbiter
#	- bootloader_config
#	- bootloader_version
#	- cache_flush
#	- codec_enabled
#	- commands
#	- disk_notify
#	- display_power
#	- dispmanx_list
#	- egl_platform_switch
#	- enable_clock
#	- file
#	- force_audio
#	- get_camera
#	- get_config
#	- get_hvs_asserts
#	- get_lcd_info
#	- get_mem
#	- get_rsts
#	- get_throttled
#	- hdmi_adjust_clock
#	- hdmi_channel_map
#	- hdmi_cvt
#	- hdmi_ntsc_freqs
#	- hdmi_status_show
#	- hdmi_stream_channels
#	- hdmi_timings
#	- hvs_update_fields
#	- inuse_notify
#	- measure_clock
#	- measure_temp
#	- measure_volts
#	- mem_oom
#	- mem_reloc_stats
#	- mem_validate
#	- memtest
#	- otp_dump
#	- pm_get_status
#	- pm_set_policy
#	- pm_show_stats
#	- pm_start_logging
#	- pm_stop_logging
#	- pmicrd
#	- pmicwr
#	- pwm_speedup
#	- read_ring_osc
#	- readmr
#	- render_bar
#	- scaling_kernel
#	- scaling_sharpness
#	- schmoo
#	- set_backlight
#	- set_logging
#	- set_vll_dir
#	- sus_is_enabled
#	- sus_status
#	- sus_stop_test_thread
#	- sus_suspend
#	- test_result
#	- vchi_test_exit
#	- vchi_test_init
#	- vcos
#	- vctest_get
#	- vctest_memmap
#	- vctest_set
#	- vctest_start
#	- vctest_stop
#	- version


show_usage() {
    echo "[usage]"
}

show_help() {
    show_usage
    echo "[help]"
}

convert_hex_to_bin() {
    hex="$1"
    hex=$(printf "$hex" | sed -r 's/.*0x(.*)/\1/' | tr 'a-f' 'A-F')
    echo "obase=2; ibase=16; $hex" | bc
}

show_throttled_status() {
    soft_temp_limit_any=
    throttling_any=
    arm_freq_cap_any=
    under_voltage_any=

    soft_temp_limit_now=
    throttling_now=
    arm_freq_cap_now=
    under_voltage_now=

    raw_data=$(vcgencmd get_throttled)
    bin_data=$(convert_hex_to_bin "$raw_data")
    i=19
    while read -n 1 b; do
        if [[ $b -eq 1 ]]; then
            case $i in
                19)
                    soft_temp_limit_any=yes
                    ;;
                18)
                    throttling_any=yes
                    ;;
                17)
                    arm_freq_cap_any=yes
                    ;;
                16)
                    under_voltage_any=yes
                    ;;
                3)
                    soft_temp_limit_now=yes
                    ;;
                2)
                    throttling_now=yes
                    ;;
                1)
                    arm_freq_cap_now=yes
                    ;;
                0)
                    under_voltage_now=yes
                    ;;
            esac
        fi
        ((i -= 1))
    done <<< "$bin_data"

    echo "Now:"
    echo "  Soft temp limit: $soft_temp_limit_now"
    echo "  Throttling:      $throttling_now"
    echo "  ARM freq. cap:   $arm_freq_cap_now"
    echo "  Under voltage:   $under_voltage_now"
    echo "Since boot:"
    echo "  Soft temp limit: $soft_temp_limit_any"
    echo "  Throttling:      $throttling_any"
    echo "  ARM freq. cap:   $arm_freq_cap_any"
    echo "  Under voltage:   $under_voltage_any"
}

show_freq() {
    freq=$(vcgencmd measure_clock core)
    echo "CPU freq: ${freq#*=}"
}

show_temp() {
    temp=$(vcgencmd measure_temp)
    echo "CPU temp: ${temp#*=}"
}

show_volts() {
    volts=$(vcgencmd measure_volts)
    echo "Voltage: ${volts#*=}"
}


# Parse arguments.
while getopts "dh" o; do
    case "${o}" in
        d) # debug
            set -x
            DEBUG=true
            ;;
        h) # help
            show_help
            exit 0
            ;;
        *) # other
            show_usage
            exit 1
            ;;
    esac
done
shift $((OPTIND-1))

# Ensure elevated privileges.
if [ $(id -u) -ne 0 ]; then
    echo "ERROR: Please run script again with root privileges."
    exit 1
fi

# Display output.
show_temp
show_volts
show_freq
echo
show_throttled_status
